FROM  python:3.9-alpine
ENV CONFIG_FILE /opt/cachet-exporter/cachet_exporter/config.yml
ENV PYTHONPATH /opt/cachet-exporter/
WORKDIR /opt/cachet-exporter
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY cachet_exporter cachet_exporter
