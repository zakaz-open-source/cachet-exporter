.PHONY: build run debug_up debug_down

build:
	docker image build -t cachet-exporter .

run_zabbix_to_cachet:
	docker container rm -f zabbix-to-cachet || true;
	docker run --name zabbix-to-cachet \
		-v $(pwd)/cachet_exporter:/cachet_exporter \
		cachet-exporter \
		sh -c "python cachet_exporter/zabbix_to_cachet.py"

debug_run:
	docker-compose up

debug_stop:
	docker-compose down
