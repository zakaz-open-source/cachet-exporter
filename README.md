# cachet-exporter

Provides an analogous functionality to [zabbix-cachet](https://github.com/qk4l/zabbix-cachet):
- exports Zabbix services to Cachet groups and components;
- exports Zabbix triggers to component statuses and incidents.

Improvements to zabbix-cachet functionality:
- allow arbitrarily deep nesting and grouping of Zabbix services;
```
root
|_ Ukraine
    |_ stores-api  # zabbix-cachet collects triggers only on this level
        |_ is-alive
            |_ is-alive-catalog  # we collect triggers everywhere under a parent
            |_ is-alive-cart
        |_ sla
            |_ catalog-sla
            |_ search-sla
```
- create Cachet groups from first-level services, create Cachet components from second-level services;
```
root
|_ Ukraine
    |_ stores-api  # we create Cachet components only for second-level services
        |_ is-alive  # zabbix-cachet creates Cachet components for all services
        |_ sla
```
- status of a component depends on a trigger or a bunch of Zabbix triggers;
```
root
|_ Ukraine  # it's exported to Cachet group
    |_ stores-api  # it's exported to Cachet component
        |_ is-alive  # this level and all leves below are not exported to components/groups
            |_ is-alive-catalog 
            |_ is-alive-cart
        |_ sla
            |_ catalog-sla  # the Cachet component's status and incidents depend on
            |_ search-sla  # all triggers located under the component
```

## Setup
### Config
This package uses the same config format, structure and instructions as zabbix-cachet. 
An example config file is located in `cachet_export/config.example.yml`.
Create copy of the example in the same directory and rename it to `config.yml`.
Replace Zabbix and Cachet URLs and credentials with a real ones in the according blocks.
Other instructions can be used out of the box.

### Build a Docker image
`Makefile` contains a shortcut for building the image: `make build`.

## Production use
`Makefile` contains a shortcut for exporting data from a Zabbix API: `run_zabbix_to_cachet`.
So that shortcut can be used or the shortcut's command can be uses as a sample.

## Debug and development
`docker-compose` and Makefile shortcuts `debug_*` are designed for development and debug.