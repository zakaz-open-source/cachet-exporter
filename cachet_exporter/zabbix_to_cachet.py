import os
import logging
import sys
from threading import Timer
from dataclasses import dataclass
from datetime import datetime
from typing import Callable, Dict, Tuple

import pytz
import yaml

from cachet_exporter import cachet as cachet_mod
from cachet_exporter import zabbix as zabbix_mod


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(name)s %(threadName)-15s - %(message)s",
)
logger = logging.getLogger("zabbix_cachet_v2")


@dataclass
class MessageTemplates:
    acknowledgment: str
    investigating: str
    resolving: str


def read_config(path: str) -> Dict:
    try:
        return yaml.safe_load(open(path))
    except (IOError, yaml.error.MarkedYAMLError) as exc:
        logger.error("Failed to parse config file %s. Error: '%s'.", path, exc)
        sys.exit(1)


ZABBIX_SEVERITY_TO_CACHET_STATUS = {
    zabbix_mod.Severity.NOT_CLASSIFIED: cachet_mod.ComponentStatus.PERFORMANCE_ISSUES,
    zabbix_mod.Severity.INFORMATION: cachet_mod.ComponentStatus.PERFORMANCE_ISSUES,
    zabbix_mod.Severity.WARNING: cachet_mod.ComponentStatus.PERFORMANCE_ISSUES,
    zabbix_mod.Severity.AVERAGE: cachet_mod.ComponentStatus.PARTIAL_OUTAGE,
    zabbix_mod.Severity.HIGH: cachet_mod.ComponentStatus.MAJOR_OUTAGE,
    zabbix_mod.Severity.DISASTER: cachet_mod.ComponentStatus.MAJOR_OUTAGE,
}


class ZabbixToCachetExporter:
    DATE_FMT = "%b %d, %H:%M"

    def __init__(
        self,
        zabbix: zabbix_mod.Zabbix,
        cachet: cachet_mod.Cachet,
        msg_templates: MessageTemplates,
        settings: Dict,
    ) -> None:
        self.zabbix = zabbix
        self.cachet = cachet
        self.msg_templates = msg_templates
        self.settings = settings

    @property
    def tz(self):
        return pytz.timezone(self.settings["time_zone"])

    def _zabbix_service_tree(self) -> zabbix_mod.Service:
        services_tree = self.zabbix.services_tree()
        root_service = self.settings["root_service"] or None
        if root_service:
            services_tree = services_tree.search(root_service)
            if services_tree is None:
                logger.error(
                    "Zabbix root_service was not found. Service name: %s.",
                    root_service,
                )
                sys.exit(1)
        return services_tree

    def export_groups(self):
        """Exports first-level Zabbix services to Cachet groups."""
        cachet_group_names = set(group.name for group in self.cachet.groups())
        zabbix_services_root = self._zabbix_service_tree()
        exported_names = []
        for zabbix_srv in zabbix_services_root.children:
            if zabbix_srv.name in cachet_group_names:  # only non-existing services
                continue
            self.cachet.create_group(
                cachet_mod.Group(
                    name=zabbix_srv.name,
                    order=zabbix_srv.sortorder,
                )
            )
            exported_names.append(zabbix_srv.name)
        logger.info(
            "Zabbix services have been exported to Cachet groups. Names: %s.",
            exported_names,
        )

    def export_components(self):
        """Exports second-level Zabbix services to Cachet components."""
        zabbix_services_root = self._zabbix_service_tree()
        cachet_groups = {group.name: group for group in self.cachet.groups()}
        cachet_components = {
            (comp.group.name, comp.name): comp for comp in self.cachet.components()
        }  # type: Dict[Tuple[str, str], cachet_mod.Component]
        exported_names = []
        for srv_1lvl in zabbix_services_root.children:
            for srv_2lvl in srv_1lvl.children:
                if (srv_1lvl.name, srv_2lvl.name) in cachet_components:
                    # if a component with the same name and the same group's name exists
                    # do not export it
                    continue
                if srv_2lvl.trigger:
                    link = srv_2lvl.trigger.url
                    description = srv_2lvl.trigger.name
                else:
                    link = None
                    description = "Contains: " + ", ".join(
                        child.trigger.name
                        for child in srv_2lvl.children_with_triggers
                    )
                self.cachet.create_component(
                    cachet_mod.Component(
                        name=srv_2lvl.name,
                        description=description,
                        order=srv_2lvl.sortorder,
                        group=cachet_groups.get(srv_1lvl.name),
                        link=link,
                    )
                )
                exported_names.append(srv_2lvl.name)
        logger.info(
            "Zabbix services have been exported to Cachet components. Names: %s.",
            exported_names,
        )

    def sync_component_statuses(self) -> None:
        """Sync Zabbix services' triggers with Cachet components and incidents."""
        zabbix_services_root = self._zabbix_service_tree()
        cachet_components = {
            (comp.group.name, comp.name): comp
            for comp in self.cachet.components()
        }  # type: Dict[Tuple[str, str], cachet_mod.Component]
        for srv_1lvl in zabbix_services_root.children:
            for srv_2lvl in srv_1lvl.children:
                cachet_component = cachet_components.get((srv_1lvl.name, srv_2lvl.name))
                if not cachet_component:
                    # service is not exported yet
                    continue
                triggers = (srv.trigger for srv in srv_2lvl.children_with_triggers)
                for trigger in triggers:
                    logger.info(
                        "Sync Zabbix trigger with Cachet component."
                        " Service: %s->%s. Trigger: %s. Trigger status: %s.",
                        srv_1lvl.name,
                        srv_2lvl.name,
                        trigger.name,
                        trigger.status.name,
                    )
                    if trigger.status == zabbix_mod.TriggerStatus.OK:
                        self._sync_trigger_status_ok(trigger, cachet_component)
                    else:
                        self._sync_trigger_status_problem(trigger, cachet_component)

    def _sync_trigger_status_ok(
        self,
        zabbix_trigger: zabbix_mod.Trigger,
        cachet_component: cachet_mod.Component,
    ) -> None:
        if cachet_component.status == cachet_mod.ComponentStatus.OPERATIONAL:
            # the component's status is OPERATIONAL, the trigger's status is OK
            # there is nothing to do
            return
        cachet_incident = next(self.cachet.incidents(cachet_component), None)
        if not cachet_incident:
            # component's status is not OPERATIONAL but no related incident record
            # let's fix components status to OPERATIONAL
            cachet_component.status = cachet_mod.ComponentStatus.OPERATIONAL
            self.cachet.update_component(cachet_component)
            return
        cachet_component_new_status = ZABBIX_SEVERITY_TO_CACHET_STATUS[zabbix_trigger.severity]
        if cachet_incident.component.status.value != cachet_component_new_status.value:
            # if the trigger severity does not match the incident's component status,
            # do not resolve the incident, wait for the appropriate trigger's severity
            return
        cachet_incident_meta = cachet_incident.message.meta
        if not cachet_incident_meta:
            # incident was not created by the script,
            # since the script adds the block "meta" in messages,
            # so incident will not be resolved by the script
            return
        if cachet_incident_meta.get("zabbix", {}).get("trigger_id") != zabbix_trigger.id:
            # if the incident was not created by the trigger,
            # do not resolve the incident, wait for the appropriate trigger
            return
        # if a component is not OPERATIONAL but the trigger is OK,
        # change components status to OPERATIONAL
        cachet_incident.component.status = cachet_mod.ComponentStatus.OPERATIONAL
        cachet_incident.status = cachet_mod.IncidentStatus.FIXED
        resolving_msg = self.msg_templates.resolving.format(
            time=datetime.utcnow().astimezone(self.tz).strftime(self.DATE_FMT)
        )
        cachet_incident.message.prepend(resolving_msg)
        self.cachet.update_incident(cachet_incident)
        logger.info(
            "Mark component as operational. Group: %s. Component: %s.",
            cachet_component.group.name,
            cachet_component.name,
        )

    def _sync_trigger_status_problem(
        self,
        zabbix_trigger: zabbix_mod.Trigger,
        cachet_component: cachet_mod.Component,
    ) -> None:
        zabbix_trigger_event = next(
            self.zabbix.trigger_events(zabbix_trigger, limit=1, sort_desc=True), None
        )
        if not zabbix_trigger_event:
            return
        incident_msg = self._cachet_incident_msg(zabbix_trigger_event, cachet_component)
        incident_msg.add_meta({"zabbix": {"trigger_id": zabbix_trigger.id}})
        if zabbix_trigger_event.acknowledged:
            incident_status = cachet_mod.IncidentStatus.IDENTIFIED
        else:
            incident_status = cachet_mod.IncidentStatus.INVESTIGATING
        cachet_incident = next(self.cachet.incidents(cachet_component), None)
        if not cachet_incident or cachet_incident.status == cachet_mod.IncidentStatus.FIXED:
            cachet_component.status = ZABBIX_SEVERITY_TO_CACHET_STATUS[zabbix_trigger.severity]
            cachet_incident_new = cachet_mod.Incident(
                name=f"{cachet_component.group.name}|{zabbix_trigger.name}",
                status=incident_status,
                component=cachet_component,
                message=incident_msg,
            )
            self.cachet.create_incident(cachet_incident_new)
            logger.info(
                "Create an incident. Name: %s. Status: %s."
                " Group: %s. Component: %s.",
                cachet_incident_new.name,
                cachet_incident_new.status,
                cachet_incident_new.component.group.name,
                cachet_incident_new.component.name,
            )
        else:
            if incident_msg == cachet_incident.message:
                # if the incident's message doesn't change,
                # do not update the incident
                return
            cachet_component_new_status = ZABBIX_SEVERITY_TO_CACHET_STATUS[zabbix_trigger.severity]
            if cachet_incident.component.status.value >= cachet_component_new_status.value:
                # if the trigger severity is lower than the incident's component status,
                # do not update the incident
                return
            cachet_incident.name = f"{cachet_component.group.name}|{zabbix_trigger.name}",
            cachet_incident.message = incident_msg
            cachet_incident.status = incident_status
            cachet_incident.component.status = ZABBIX_SEVERITY_TO_CACHET_STATUS[zabbix_trigger.severity]
            self.cachet.update_incident(cachet_incident)
            logger.info(
                "Update the incident. Name: %s. Status: %s."
                " Group: %s. Component: %s.",
                cachet_incident.name,
                cachet_incident.status,
                cachet_incident.component.group.name,
                cachet_incident.component.name,
            )

    def _cachet_incident_msg(
        self,
        zabbix_trigger_event: zabbix_mod.TriggerEvent,
        cachet_component: cachet_mod.Component,
    ) -> cachet_mod.IncidentMessage:
        if zabbix_trigger_event.acknowledged:
            content = "".join(
                self.msg_templates.acknowledgment.format(
                    message=ack.message,
                    ack_time=ack.created_at.astimezone(self.tz).strftime(self.DATE_FMT),
                    author=ack.author_name,
                    trigger_name=zabbix_trigger_event.trigger.name,
                    trigger_description=zabbix_trigger_event.trigger.description,
                )
                for ack in reversed(zabbix_trigger_event.acknowledges)
            )
        elif self.msg_templates.investigating:
            created_at = zabbix_trigger_event.created_at.astimezone(self.tz)
            content = self.msg_templates.investigating.format(
                group=cachet_component.group.name,
                component=cachet_component.name,
                time=created_at.strftime(self.DATE_FMT),
                trigger_name=zabbix_trigger_event.trigger.name,
                trigger_description=zabbix_trigger_event.trigger.description,
            )
        elif zabbix_trigger_event.trigger.description:
            content = zabbix_trigger_event.trigger.description
        else:
            content = zabbix_trigger_event.trigger.name
        return cachet_mod.IncidentMessage(content)


def periodic(
    interval: int, function: Callable, args: Tuple = tuple()
) -> None:
    try:
        function(*args)
    except Exception as e:
        logger.exception(e)
    timer = Timer(interval, periodic, args=(interval, function, args))
    timer.name = function.__name__
    timer.start()


def main():
    config = read_config(os.environ.get('CONFIG_FILE'))
    ZABBIX = config['zabbix']
    zabbix = zabbix_mod.Zabbix(ZABBIX['server'], ZABBIX['user'], ZABBIX['pass'])
    CACHET = config['cachet']
    cachet = cachet_mod.Cachet(
        CACHET['server'], CACHET['token'], CACHET['https-verify']
    )
    SETTINGS = config["settings"]
    logging.getLogger("requests").setLevel(SETTINGS["log_level_requests"])
    TEMPLATES = config["templates"]
    exporter = ZabbixToCachetExporter(
        zabbix=zabbix,
        cachet=cachet,
        msg_templates=MessageTemplates(
            acknowledgment=TEMPLATES["acknowledgement"],
            investigating=TEMPLATES["investigating"],
            resolving=TEMPLATES["resolving"],
        ),
        settings=SETTINGS,
    )
    periodic(SETTINGS["update_comp_interval"], exporter.export_groups)
    periodic(SETTINGS["update_comp_interval"], exporter.export_components)
    periodic(SETTINGS["update_inc_interval"], exporter.sync_component_statuses)


if __name__ == '__main__':
    main()
