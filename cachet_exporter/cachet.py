import enum
import json
import logging
import re
from dataclasses import dataclass, asdict
from functools import cached_property
from typing import Dict, Iterator, Optional, List, Tuple
from urllib.parse import urljoin

import requests


logger = logging.getLogger(__name__)

Page = int


class GroupCollapsed(enum.Enum):
    NO = 0
    YES = 1
    IF_A_COMPONENT_NOT_OPERATIONAL = 2


@dataclass
class Group:
    name: str
    order: int
    collapsed: GroupCollapsed = GroupCollapsed.IF_A_COMPONENT_NOT_OPERATIONAL
    id: Optional[int] = None

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return False
        return asdict(self) == asdict(other)

    @classmethod
    def from_raw(cls, raw_group) -> "Group":
        return cls(
            id=raw_group["id"],
            name=raw_group["name"],
            collapsed=GroupCollapsed(raw_group["collapsed"]),
            order=raw_group["order"],
        )

    def to_raw(self) -> Dict:
        raw = asdict(self)
        raw["collapsed"] = raw["collapsed"].value
        return raw


class ComponentStatus(enum.Enum):
    # https://docs.cachethq.io/docs/component-statuses
    OPERATIONAL = 1
    PERFORMANCE_ISSUES = 2
    PARTIAL_OUTAGE = 3
    MAJOR_OUTAGE = 4


@dataclass
class Component:
    name: str
    description: str
    order: int
    group: Optional[Group] = None
    status: ComponentStatus = ComponentStatus.OPERATIONAL
    link: Optional[str] = None
    id: Optional[int] = None

    @classmethod
    def from_raw(cls, raw_component, group: Optional[Group] = None) -> "Component":
        return cls(
            id=raw_component["id"],
            name=raw_component["name"],
            description=raw_component["description"],
            status=ComponentStatus(raw_component["status"]),
            order=raw_component["order"],
            link=raw_component["link"],
            group=group,
        )

    def to_raw(self) -> Dict:
        raw = asdict(self)
        raw["status"] = raw["status"].value
        if group := raw.pop("group", None):
            raw["group_id"] = group["id"]
        return raw


class IncidentStatus(enum.Enum):
    # https://docs.cachethq.io/docs/incident-statuses
    SCHEDULED = 0
    INVESTIGATING = 1
    IDENTIFIED = 2
    FIXED = 4


class IncidentMessage:
    META_RE = re.compile(r"\[meta]: # \((?P<body>.+?)\)")

    def __init__(self, content: str) -> None:
        self.content = content

    def __str__(self) -> str:
        return self.content

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return False
        return self.content.strip() == other.content.strip()

    def prepend(self, content: str) -> None:
        self.content = content + self.content

    def add_meta(self, meta: Dict) -> None:
        if self.meta:
            # unset existing meta
            self.content = self.META_RE.sub(self.content, "")
        self.content += f'\n[meta]: # ({json.dumps(meta)})'

    @property
    def meta(self) -> Optional[Dict]:
        match = self.META_RE.search(self.content)
        if not match:
            return None
        return json.loads(match.group("body"))


@dataclass
class Incident:
    name: str
    message: IncidentMessage
    status: IncidentStatus
    component: Component
    id: Optional[int] = None

    @classmethod
    def from_raw(cls, raw_incident: Dict, component: Component) -> "Incident":
        return cls(
            id=raw_incident["id"],
            name=raw_incident["name"],
            message=IncidentMessage(raw_incident["message"]),
            component=component,
            status=IncidentStatus(raw_incident["status"]),
        )

    def to_raw(self) -> Dict:
        raw = asdict(self)
        raw["message"] = raw["message"].content
        raw["status"] = raw["status"].value
        raw_component = raw.pop("component")
        raw["component_id"] = raw_component["id"]
        raw["component_status"] = raw_component["status"].value
        return raw


class Cachet:
    def __init__(self, api_url: str, token: str, https_verify: bool = True) -> None:
        self.api_url = urljoin(api_url, "/api/v1/")
        self.token = token
        self.https_verify = https_verify

    def __repr__(self) -> str:
        args = (self.api_url, self.token, self.https_verify)
        return f"{type(self).__name__}{args}"

    @cached_property
    def _api_session(self) -> requests.Session:
        session = requests.Session()
        session.headers.update({"X-Cachet-Token": self.token})
        session.verify = self.https_verify
        return session

    def _items_chunk(self, path: str, page: Page = 1, filters: Dict = None) -> Tuple[List[Dict], Page]:
        url = urljoin(self.api_url, path)
        params = {"per_page": 50, "page": page}
        resp = self._api_session.get(url, params=params)
        payload = json.loads(resp.text)
        try:
            items, total_pages = payload["data"], payload["meta"]["pagination"]["total_pages"]
        except KeyError as exc:
            logging.error("Cachet invalid response %s: %s.", url, payload)
            raise exc
        if filters:
            filtered_items = []
            for field, value in filters.items():
                for item in items:
                    if item.get(field) == value:
                        filtered_items.append(item)
            items = filtered_items
        return items, total_pages

    def _items(self, path: str, filters: Dict = None) -> Iterator[Dict]:
        page = 1
        while page:
            chunk, total_pages = self._items_chunk(path, page, filters)
            page = page + 1 if total_pages > page else None
            yield from chunk

    def _items_desc(self, path: str, filters: Dict = None) -> Iterator[Dict]:
        chunk, total_pages = self._items_chunk(path, filters=filters)
        if total_pages == 1:
            yield from reversed(chunk)
            return
        page = total_pages
        while page:
            chunk, _ = self._items_chunk(path, page, filters)
            page = page - 1 if page > 1 else None
            yield from reversed(chunk)

    def _create(self, path: str, payload: Dict) -> Dict:
        url = urljoin(self.api_url, path)
        resp = self._api_session.post(url, payload)
        resp.raise_for_status()
        return json.loads(resp.text)

    def _update(self, path: str, payload: Dict) -> Dict:
        url = urljoin(self.api_url, path)
        resp = self._api_session.put(url, payload)
        resp.raise_for_status()
        return json.loads(resp.text)

    def groups(self) -> Iterator[Group]:
        for raw_group in self._items("components/groups"):
            yield Group.from_raw(raw_group)

    def create_group(self, group: Group) -> None:
        self._create("components/groups/", group.to_raw())
        # raw_group = json.loads(self._api_session.post(url, group.to_raw()).text)
        # return Group.from_raw(raw_group["data"])

    def components(self) -> Iterator[Component]:
        groups = {group.id: group for group in self.groups()}
        for raw_component in self._items("components"):
            yield Component.from_raw(raw_component, groups.get(raw_component["group_id"]))

    def create_component(self, component: Component) -> None:
        self._create("components/", component.to_raw())

    def update_component(self, component: Component) -> None:
        self._update(f"components/{component.id}/", component.to_raw())

    def incidents(self, component: Component) -> Iterator[Incident]:
        filters = {"component_id": component.id}
        for raw_group in self._items_desc("incidents", filters=filters):
            yield Incident.from_raw(raw_group, component)

    def create_incident(self, incident: Incident) -> None:
        self._create("incidents/", incident.to_raw())

    def update_incident(self, incident: Incident) -> None:
        self._update(f"incidents/{incident.id}/", incident.to_raw())
