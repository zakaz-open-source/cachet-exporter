import enum
from dataclasses import asdict, dataclass, field as dataclass_field
from datetime import datetime
from functools import cached_property
from typing import Callable, Dict, Iterator, Iterable, List, Optional, Set

from pyzabbix import ZabbixAPI


class Severity(enum.Enum):
    NOT_CLASSIFIED = "0"
    INFORMATION = "1"
    WARNING = "2"
    AVERAGE = "3"
    HIGH = "4"
    DISASTER = "5"


class TriggerStatus(enum.Enum):
    OK = "0"
    PROBLEM = "1"


@dataclass
class Trigger:
    id: str
    name: str
    description: str
    url: str
    severity: Severity
    status: TriggerStatus

    @classmethod
    def from_raw(cls, raw_trigger: Dict) -> "Trigger":
        return cls(
            id=raw_trigger["triggerid"],
            name=raw_trigger["description"],
            description=raw_trigger["comments"],
            url=raw_trigger["url"],
            severity=Severity(raw_trigger["priority"]),
            status=TriggerStatus(raw_trigger["value"]),
        )


@dataclass
class Service:
    id: str
    name: str
    status: Severity
    sortorder: int = 0
    trigger: Optional[Trigger] = None
    parents: Set["Service"] = dataclass_field(default_factory=set)
    children: Set["Service"] = dataclass_field(default_factory=set)

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return False
        return asdict(self) == asdict(other)

    @classmethod
    def from_raw(cls, raw_service: Dict) -> "Service":
        return cls(
            id=raw_service["serviceid"],
            name=raw_service["name"],
            status=Severity(raw_service["status"]),
            sortorder=raw_service["sortorder"],
        )

    @property
    def children_with_triggers(self) -> Iterator["Service"]:
        """According to doc: At the lowest level of services are triggers."""
        yield from self._children_filtered(self, lambda srv: srv.trigger is not None)

    def _children_filtered(
        self, srv: "Service", predicate: Callable[["Service"], bool]
    ) -> Iterator["Service"]:
        for child in srv.children:
            if child.children:
                yield from self._children_filtered(child, predicate)
            if predicate(child):
                yield child
        if predicate(srv):
            yield srv

    def search(self, service_name: str) -> "Service":
        if service_name == self.name:
            return self

        def _search(services: Iterable[Service]) -> Optional[Service]:
            for service in services:
                if service.name == service_name:
                    return service
            for service in services:
                if service.children:
                    return _search(service.children)
            return None

        return _search(self.children)


class EventObject(enum.Enum):
    TRIGGER = 0


@dataclass
class EventAcknowledge:
    id: str
    message: str
    author_name: str
    created_at: datetime

    @classmethod
    def from_raw(cls, raw_acknowledge: Dict) -> "EventAcknowledge":
        return cls(
            id=raw_acknowledge["acknowledgeid"],
            message=raw_acknowledge["message"],
            author_name=raw_acknowledge.get("name") + " " + raw_acknowledge.get("surname"),
            created_at=datetime.fromtimestamp(int(raw_acknowledge["clock"])),
        )


@dataclass
class TriggerEvent:
    id: str
    trigger: Trigger
    acknowledged: bool
    acknowledges: List[EventAcknowledge]
    created_at: datetime

    @classmethod
    def from_raw(cls, raw_event: Dict, trigger: Trigger) -> "TriggerEvent":
        return cls(
            id=raw_event["eventid"],
            trigger=trigger,
            acknowledged=bool(int(raw_event["acknowledged"])),
            acknowledges=[
                EventAcknowledge.from_raw(ack)
                for ack in raw_event["acknowledges"]
            ],
            created_at=datetime.fromtimestamp(int(raw_event["clock"])),
        )


class Zabbix:
    def __init__(self, api_url: str, username: str, pwd: str, https_verify: bool = True) -> None:
        self.api_url = api_url
        self.username = username
        self.pwd = pwd
        self.https_verify = https_verify

    def __repr__(self) -> str:
        args = (self.api_url, self.username, self.pwd, self.https_verify)
        return f"{type(self).__name__}{args}"

    @cached_property
    def _api(self) -> ZabbixAPI:
        api = ZabbixAPI(self.api_url)
        api.session.verify = self.https_verify
        api.login(self.username, self.pwd)
        return api

    def services(self) -> List[Service]:
        query_params = {"selectParentDependencies": "extend", "selectDependencies": "extend"}
        resp = self._api.do_request(method="service.get", params=query_params)
        raw_services = {srv["serviceid"]: srv for srv in resp["result"]}
        trigger_ids = set(
            raw_srv["triggerid"]
            for raw_srv in raw_services.values()
            if raw_srv["triggerid"]
        )
        triggers = {trig.id: trig for trig in self.triggers(trigger_ids)}
        services = {srv_id: Service.from_raw(srv) for srv_id, srv in raw_services.items()}
        for srv in services.values():
            raw_srv = raw_services[srv.id]
            # set trigger
            trigger_id = raw_srv["triggerid"]
            if trigger_id != "0":  # no associated trigger
                srv.trigger = triggers[trigger_id]
            # set parents
            parent_ids = set(
                dep["serviceupid"]
                for dep in raw_srv.get("parentDependencies", [])
            )
            for parent_id in parent_ids:
                srv.parents.add(services[parent_id])
            # set children
            child_ids = set(
                dep["servicedownid"]
                for dep in raw_srv.get("dependencies", [])
            )
            for child_id in child_ids:
                srv.children.add(services[child_id])
        return list(services.values())

    def services_tree(self) -> Service:
        first_level_services = [
            srv for srv in self.services() if not srv.parents
        ]
        abc_root_service = Service(
            id="(abstract entity)",
            name="__root__",
            status=Severity.NOT_CLASSIFIED,
            children=set(first_level_services),
        )
        for srv in first_level_services:
            srv.parents.add(abc_root_service)
        return abc_root_service

    def triggers(self, trigger_ids: Iterable[str]) -> List[Trigger]:
        query_params = {"triggerids": list(trigger_ids)}
        resp = self._api.do_request(method="trigger.get", params=query_params)
        return [Trigger.from_raw(trig) for trig in resp["result"]]

    def trigger_events(
        self, trigger: Trigger, limit: int = 10, sort_desc: bool = False
    ) -> Iterator[TriggerEvent]:
        query_params = {
            "select_acknowledges": "extend",
            "object": EventObject.TRIGGER.value,
            "value": TriggerStatus.PROBLEM.value,
            "objectids": trigger.id,
            "limit": limit,
            "sortfield": "clock",
            "sortorder": "DESC" if sort_desc else "ASC",
        }
        resp = self._api.do_request(method="event.get", params=query_params)
        return (TriggerEvent.from_raw(event, trigger) for event in resp["result"])
